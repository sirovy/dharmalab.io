#!/usr/bin/env python3
#
# MP3 Nocni dharma downloader
# ===========================
# - download all nocni dharma talks from radio1 archiv to current directory
# - require python3 
# - tested only on Linux OS
#
# Marek Sirovy (msirovy _at_ gmail.com)


from urllib.request import urlopen, Request
import re
from os import path, curdir


URL="http://www.radio1.cz/archiv-poradu/download-list/35-pulnocni-dharma?perpage=500"


def save_mp3(url, file_name):
    """ Get url and save it to file as binary stream
    """
    with open(file_name, 'wb') as F:
        req = Request(url)
        data = urlopen(req).read()
        F.write(data)
    return True


if __name__ == '__main__':    
    _req = Request(URL)
    _WORK_DIR = path.abspath(curdir())
    
    for link in re.findall( r'<a href="/archiv-poradu/(.*?)mp3">', str(urlopen(_req).read()) )[::-1]:
        """ Iterate over all parsed mp3 links from given url
            and store them as dharma-ROK-MESIC_DEN.mp3 files
        """
        mp3_file_path = path.join(_WORK_DIR, 
                                "%s.mp3" % str(re.findall(r'pulnocni-(.*?)-$', link)[0]) 
                                )

        if not isfile(mp3_file_path):
            print("Downloading: ", title, "http://www.radio1.cz/archiv-poradu/%smp3" % link)
            save_mp3(
                url="http://www.radio1.cz/archiv-poradu/%smp3" % link,
                file_name=mp3_file_path
                )

